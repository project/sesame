<?php
/**
 * sesame.admin.inc - Sesame Connector admin page callbacks.
 *
 * @author Arto Bendiken <http://bendiken.net/>
 * @copyright Copyright (c) 2007-2008 Arto Bendiken. All rights reserved.
 * @license GPL <http://creativecommons.org/licenses/GPL/2.0/>
 * @package sesame.module
 */

//////////////////////////////////////////////////////////////////////////////
// Sesame Connector settings form

function sesame_admin_settings() {
  $form = array();

  // TODO

  return system_settings_form($form);
}
