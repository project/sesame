
Sesame Connector
================
This is a development version of the Sesame Connector module currently being
developed for Drupal 6.x under the auspices of the project at:

  <http://drupal.org/project/sesame>


BUG REPORTS
-----------
Post bug reports and feature requests to the issue tracking system at:

  <http://drupal.org/node/add/project_issue/sesame>


CREDITS
-------
Developed and maintained by Arto Bendiken <http://bendiken.net/>
Sponsored by MakaluMedia Group <http://www.makalumedia.com/>
Sponsored by M.C. Dean, Inc. <http://www.mcdean.com/>
Sponsored by SPAWAR <http://www.spawar.navy.mil/>
